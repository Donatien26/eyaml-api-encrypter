FROM alpine

RUN apk update
RUN apk add ruby
RUN gem install hiera-eyaml
RUN apk add openjdk11

EXPOSE 8080
COPY target/eyaml-encrypter-api-0.0.1-SNAPSHOT.jar app.jar


ENTRYPOINT [ "java","-jar",'app.jar' ]