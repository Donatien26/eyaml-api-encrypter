package com.example.eyamlencrypterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EyamlEncrypterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EyamlEncrypterApiApplication.class, args);
	}

}
