package com.example.eyamlencrypterapi.controllers;

import com.example.eyamlencrypterapi.model.EncryptRequest;
import com.example.eyamlencrypterapi.services.EncrypterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "Encrypt a secret with the provided key", description = "Enpoint to encrypt data with eyaml")
@RequestMapping(value = { "/" })
public class EncryptController {

    @Autowired
    private EncrypterService encrypterService;

    @GetMapping(path = { "/encrypt-property" })
    @Operation(summary = "Encrypt a secret with the provided key")
    public String encryptProperty(@RequestBody EncryptRequest request) {
        return encrypterService.encryptSecret(request.getProperty_key(), request.getProperty_value(),
                request.getPublic_key());
    }

    @GetMapping(path = { "/keys" })
    @Operation(summary = "List available keys on server")
    public String listKeys(@RequestBody EncryptRequest request) {
        return encrypterService.getKeys();
    }

}
