package com.example.eyamlencrypterapi.services.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

import com.example.eyamlencrypterapi.services.EncrypterService;
import com.example.eyamlencrypterapi.utils.Command;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessResult;

@Service
public class EncrypterServiceImpl implements EncrypterService {

    @Value("${com.example.eyaml-encrypter-api.path_to_keys:}")
    private String path_to_keys;

    @Override
    public String encryptSecret(String property, String value, String private_key) {
        try {
            ProcessResult res = Command.getProcessExecutor().command("eyaml", "encrypt", "-l" + property, "-s" + value,
                    "--pkcs7-public-key", "/workspaces/eyaml-api-encrypter/keys/public_key.pem").execute();
            return res.getOutput().getString(StandardCharsets.UTF_8.name());
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public String getKeys() {
        try {
            ProcessResult test = Command.getProcessExecutor().command("cd", path_to_keys, "&&", "ls", "-a").execute();
            return test.getOutput().getString(StandardCharsets.UTF_8.name());
        } catch (InvalidExitValueException | IOException | InterruptedException | TimeoutException e) {
            return e.getMessage();
        }
    }

}
