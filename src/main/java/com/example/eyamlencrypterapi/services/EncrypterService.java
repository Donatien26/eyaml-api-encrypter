package com.example.eyamlencrypterapi.services;

public interface EncrypterService {

    String encryptSecret(String property, String value, String private_key);

    String getKeys();
}
