package com.example.eyamlencrypterapi.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.listener.ProcessListener;

public class Command {
    private static Logger LOGGER = LoggerFactory.getLogger(Command.class);

    public static ProcessExecutor getProcessExecutor() {
        ProcessExecutor processExecutor = new ProcessExecutor();
        processExecutor.redirectError(System.err);
        processExecutor.readOutput(true);
        processExecutor.addListener(new ProcessListener() {
            @Override
            public void afterStart(Process process, ProcessExecutor executor) {
                process.info().commandLine().ifPresent(cli -> LOGGER.info(cli));
                super.afterStart(process, executor);
            }
        });
        return processExecutor;
    }

}
